module.exports = (sequelize, DataTypes) => {
  const UserGroup = sequelize.define('UserGroup', {
    UserId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    GroupId: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  })

  UserGroup.associate = function (models) {
    UserGroup.belongsTo(models.User, { foreignKey: 'UserId' })
    UserGroup.belongsTo(models.Group, { foreignKey: 'GroupId' })
  }

  return UserGroup
}
