const Promise = require('bluebird')
const bcrypt = Promise.promisifyAll(require('bcrypt'))

function hashPassword (user, option) {
  const SALT_FACTOR = 8

  if (!user.changed('password')) {
    return
  }

  return bcrypt
    .genSalt(SALT_FACTOR)
    .then(salt => bcrypt.hash(user.password, salt, null))
    .then(hash => {
      user.setDataValue('password', hash)
    })
}

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    firstname: {
      type: DataTypes.STRING,
      allowNull: false
    },
    lastname: {
      type: DataTypes.STRING,
      allowNull: false
    },
    role: {
      type: DataTypes.STRING,
      enum: [
        'superuser',
        'admin',
        'headmaster',
        'teacher',
        'parent',
        'student'
      ],
      default: 'student',
      allowNull: false
    }
    /*
    groups: {
      type: DataTypes.STRING,
      get: function () {
        return JSON.parse(this.getDataValue('groups'))
      },
      set: function (val) {
        return this.setDataValue('groups', JSON.stringify(val))
      }
    }

    TODO - dynamic permissions
    ,
    canReadOwnProfile: {
      type: DataTypes.BOOLEAN,
      default: true,
      allowNull: false
    },
    canEditOwnProfile: {
      type: DataTypes.BOOLEAN,
      default: true,
      allowNull: false
    },
    canReadAnySchoolProfile: {
      type: DataTypes.BOOLEAN,
      default: true,
      allowNull: false
    },
    canEditAnySchoolProfile: {
      type: DataTypes.BOOLEAN,
      default: true,
      allowNull: false
    },
    canEditAnyProfile: {
      type: DataTypes.BOOLEAN,
      default: true,
      allowNull: false
    },
    canEditAnyProfile: {
      type: DataTypes.BOOLEAN,
      default: true,
      allowNull: false
    }
    */
  }, {
    hooks: {
      beforeCreate: hashPassword,
      beforeUpdate: hashPassword,
      beforeSave: hashPassword
    }
  })

  User.associate = function (models) {
    User.belongsToMany(models.Group, { through: 'UserGroups', foreignKey: 'UserId', as: 'groups' })
  }

  User.prototype.comparePassword = function (password) {
    return bcrypt.compare(password, this.password)
  }

  return User
}
