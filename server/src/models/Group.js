module.exports = (sequelize, DataTypes) => {
  const Group = sequelize.define('Group', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    type: {
      type: DataTypes.STRING,
      enum: [
        'trieda',
        'kabinet'
      ],
      allowNull: false,
      default: 'trieda'
    }
  })

  Group.associate = function (models) {
    Group.belongsToMany(models.User, { through: 'UserGroups', foreignKey: 'GroupId', as: 'users' })
  }

  return Group
}
