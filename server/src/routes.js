const AuthenticationController = require('./controlers/AuthenticationController')
const SchoolController = require('./controlers/SchoolController')
const GroupController = require('./controlers/GroupController')
const UserController = require('./controlers/UserController')

const AuthenticationControllerPolicy = require('./policies/AutheticationControllerPolicy')
const isAutheticated = require('./policies/isAuthenticated')

module.exports = (app) => {
  app.post('/register',
    AuthenticationControllerPolicy.register,
    AuthenticationController.register)

  app.post('/login',
    AuthenticationController.login)

  app.post('/user/:id/edit',
    isAutheticated,
    UserController.edit)

  app.post('/school/new',
    isAutheticated,
    SchoolController.register)

  app.post('/school/:id',
    isAutheticated,
    SchoolController.get)

  app.post('/school/:id/edit',
    isAutheticated,
    SchoolController.edit)

  app.post('/group/new',
    isAutheticated,
    GroupController.new)

  app.post('/group/:id',
    isAutheticated,
    GroupController.view)

  app.post('/group/:id/adduser/:userid',
    isAutheticated,
    GroupController.addUser)
}
