const { School } = require('../models')

module.exports = {
  async register (req, res) {
    try {
      const school = await School.create(req.body)
      if (res) {
        res.send(school.toJSON())
      }
      return school.toJSON()
    } catch (err) {
      if (res) {
        res.status(400).send({
          error: 'Táto škola už je registrovaná.'
        })
      }
      return {
        error: 'Táto škola už je registrovaná.'
      }
    }
  },
  async get (req, res) {
    try {
      const { id } = req.params
      const school = await School.findOne({
        where: {
          id
        }
      })

      if (!school) {
        if (res) {
          return res.status(403).send({
            error: 'Nesprávne ID školy.'
          })
        }
        return {
          error: 'Nesprávne ID školy.'
        }
      }

      const schoolJson = school.toJSON()
      if (res) {
        res.send({
          school: schoolJson
        })
      }
      return {
        school: schoolJson
      }
    } catch (err) {
      if (res) {
        res.status(500).send({
          error: 'Nastala chyba pri identifikácii školy.'
        })
      }
      return {
        error: 'Nastala chyba pri identifikácii školy.'
      }
    }
  },
  async edit (req, res) {
    try {
      const school = await School.update(req.body, {
        where: {
          id: req.params.id
        }
      })
      res.send({ ...req.body, returned: !!school })
    } catch (err) {
      res.status(500).send({
        error: 'Chyba pri úprave školy.'
      })
    }
  }
}
