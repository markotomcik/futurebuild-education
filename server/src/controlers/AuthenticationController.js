const { User, Group } = require('../models')
const SchoolController = require('./SchoolController')
const jwt = require('jsonwebtoken')
const config = require('../config/config')
const { Op } = require('sequelize')

function jwtSignUser (user) {
  const ONE_WEEK = 60 * 60 * 24 * 7
  return jwt.sign(user, config.authentication.jwtSecret, {
    expiresIn: ONE_WEEK
  })
}

module.exports = {
  async register (req, res) {
    try {
      let answer = null
      let body = req.body

      if (!req.body.SchoolId) {
        answer = await SchoolController.register({
          body: req.body.school
        })
        if (answer.error) {
          return res.status(400).send({ error: answer.error })
        }
        body = { ...req.body, SchoolId: answer.id }
      } else {
        answer = await SchoolController.get({
          params: {
            id: req.body.SchoolId
          }
        })
        if (answer.error) {
          return res.status(400).send({ error: answer.error })
        }
      }

      const user = await User.create(body)

      if (req.body.GroupIds) {
        answer = await Group.findAll({
          where: {
            id: {
              [Op.or]: req.body.GroupIds
            }
          }
        })
        if (answer.error) {
          return res.status(400).send({ error: answer.error })
        }
        await user.addGroups(answer)
      }

      res.send(user.toJSON())
    } catch (err) {
      res.status(400).send({
        error: 'Táto emailová adresa už je registrovaná.'
      })
    }
  },
  async login (req, res) {
    try {
      const { email, password } = req.body
      const user = await User.findOne({
        where: {
          email: email
        },
        include: Group
      })

      if (!user) {
        return res.status(403).send({
          error: 'Nesprávne prihlasovacie údaje'
        })
      }

      const isPasswordValid = user.comparePassword(password)
      if (!isPasswordValid) {
        return res.status(403).send({
          error: 'Nesprávne prihlasovacie údaje.'
        })
      }

      const userJson = user.toJSON()
      res.send({
        user: userJson,
        token: jwtSignUser(userJson)
      })
    } catch (err) {
      res.status(500).send({
        error: 'Nastala chyba pri prihlasovaní.'
      })
    }
  }
}
