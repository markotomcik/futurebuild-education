const { User, Group } = require('../models')

module.exports = {
  async new (req, res) {
    try {
      const group = await Group.create(req.body)
      if (res) {
        res.send(group.toJSON())
      }
      return group.toJSON()
    } catch (err) {
      if (res) {
        res.status(400).send({
          error: 'Táto skupina už existuje.'
        })
      }
      return {
        error: 'Táto skupina už existuje.'
      }
    }
  },
  async view (req, res) {
    try {
      const { id } = req.params
      const group = await Group.findOne({
        where: {
          id
        }
      })

      if (!group) {
        if (res) {
          return res.status(403).send({
            error: 'Nesprávne ID skupiny.'
          })
        }
        return {
          error: 'Nesprávne ID skupiny.'
        }
      }

      const groupJson = group.toJSON()

      if (res) {
        res.send({
          group: groupJson
        })
      }
      return {
        group: groupJson
      }
    } catch (err) {
      if (res) {
        res.status(500).send({
          error: 'Nastala chyba pri identifikácii skupiny.'
        })
      }
      return {
        error: 'Nastala chyba pri identifikácii skupiny.'
      }
    }
  },
  async edit (req, res) {
    try {
      const group = await Group.update(req.body, {
        where: {
          id: req.params.id
        }
      })
      res.send({ ...req.body, returned: !!group })
    } catch (err) {
      res.status(500).send({
        error: 'Chyba pri úprave skupiny.'
      })
    }
  },
  async addUser (req, res) {
    try {
      const user = await User.findOne({
        where: {
          id: req.params.userid
        }
      })
      const group = await Group.findOne({
        where: {
          id: req.params.id
        }
      })
      await user.addGroup(group)
      res.send(group.toJSON())
    } catch (err) {
      res.status(500).send({
        error: 'Chyba pri pridávaní užívateľa do skupiny'
      })
    }
  }
}
