const Joi = require('joi')

module.exports = {
  register (req, res, next) {
    const passwordPattern = '^[a-zA-Z0-9]{8,32}$'
    const schema = Joi.object({
      email: Joi.string().email().required(),
      password: Joi.string().regex(
        new RegExp(passwordPattern)
      ).required(),
      firstname: Joi.string().required(),
      lastname: Joi.string().required(),
      SchoolId: Joi.number(),
      GroupIds: Joi.array(),
      role: Joi.string().valid(
        'superuser',
        'admin',
        'headmaster',
        'teacher',
        'parent',
        'student'
      ),
      school: Joi.object()
    })

    const { error } = schema.validate(req.body)

    if (error) {
      switch (error.details[0].context.key) {
        case 'email':
          res.status(400).send({
            error: 'Zadajte platnú emailovú adresu.'
          })
          break
        case 'password':
          res.status(400).send({
            error: 'Zadajte heslo, ktoré je bezpečné.'
          })
          break
        case 'firstname':
          res.status(400).send({
            error: 'Zadajte svoje meno.'
          })
          break
        case 'lastname':
          res.status(400).send({
            error: 'Zadajte svoje priezvisko.'
          })
          break
        case 'SchoolId':
          res.status(400).send({
            error: 'Zadajte školu, ktorá je registrovaná.'
          })
          break
        case 'GroupIds':
          res.status(400).send({
            error: 'Zadajte skupiny.'
          })
          break
        case 'role':
          res.status(400).send({
            error: 'Zadajte funkčnú rolu.'
          })
          break
        case 'groups':
          res.status(400).send({
            error: 'Zadajte funkčnú rolu.'
          })
          break
        case 'school':
          res.status(400).send({
            error: 'Zadajte školu, ktorá nie je registrovaná.'
          })
          break
        default:
          res.status(400).send({
            error: 'Nesprávne registračné údaje.'
          })
      }
    } else {
      next()
    }
  }
}
